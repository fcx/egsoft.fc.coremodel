﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EGSoft.Fc.CoreModel
{
    public class FcCommandArguments : IFcCommandArguments
    {
        FcDictionary<object> argsMap;


        public FcCommandArguments()
        {
            argsMap = new FcDictionary<object>();
        }

        public AType Get<AType>(String key)
        {
            object obj;
            if (argsMap.TryGetValue(key, out obj))
            {
                return (AType)obj;
            }
            else
            {
                throw new Exception("Argument not found!");
            }
        }

        public void Add(String name, String alias, object value)
        {
            argsMap.Add(name, alias, value);
        }
    }
}
