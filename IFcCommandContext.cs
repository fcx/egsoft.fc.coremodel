﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using EGSoft.Fc.SubspaceModel;

namespace EGSoft.Fc.CoreModel
{
    public interface IFcCommandContext
    {
        String Username { get; }
        IFcCommandArguments Arguments { get; }

        void Reply(string message);
        void Reply(string message, SoundType soundType);
    }
}
