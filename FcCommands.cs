﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EGSoft.Fc.CoreModel
{
    public class FcCommands : IFcCommands
    {
        Dictionary<String, IFcCommand> cmdNameToCmd;
        Dictionary<String, IFcCommand> cmdAliasToCmd;

        public FcCommands()
        {
            cmdNameToCmd = new Dictionary<string, IFcCommand>(StringComparer.OrdinalIgnoreCase);
            cmdAliasToCmd = new Dictionary<string, IFcCommand>(StringComparer.OrdinalIgnoreCase);
        }

        public IFcCommand Register(IFcCommand cmd)
        {
            cmdNameToCmd.Add(cmd.Name, cmd);
            cmdAliasToCmd.Add(cmd.Alias, cmd);
            return cmd;
        }


        public bool TryGetCommand(string cmdKey, out IFcCommand cmd)
        {
            if (cmdNameToCmd.TryGetValue(cmdKey, out cmd)
                || cmdAliasToCmd.TryGetValue(cmdKey, out cmd))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public IEnumerator<IFcCommand> GetEnumerator()
        {
            return cmdNameToCmd.Values.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return cmdNameToCmd.Values.GetEnumerator();
        }
    }
}
