﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EGSoft.Fc.CoreModel
{
    public interface IFcCommandParameter
    {
        bool IsRequired { get; }
        String Name { get; }
        String Alias { get; }
        String Description { get; }

        object DefaultValue { get; }


        bool TryParse(String input, out object output);

    }
}
