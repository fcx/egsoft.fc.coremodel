﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EGSoft.Fc.CoreModel
{
    internal class FcDictionary<ValueType> : IEnumerable<KeyValuePair<String, ValueType>>
    {
        Dictionary<String, ValueType> keyNameToValue;
        Dictionary<String, ValueType> keyAliasToValue;

        public FcDictionary()
        {
            keyNameToValue = new Dictionary<String, ValueType>(StringComparer.OrdinalIgnoreCase);
            keyAliasToValue = new Dictionary<String, ValueType>(StringComparer.OrdinalIgnoreCase);
        }

        public int Count
        {
            get { return keyNameToValue.Count; }
        }

        public Dictionary<String, ValueType>.ValueCollection Values
        {
            get { return keyNameToValue.Values; }
        }

        public void Add(String name, String alias, ValueType value)
        {
            keyNameToValue.Add(name, value);
            keyAliasToValue.Add(alias, value);
        }

        public bool TryGetValue(String nameOrAlias, out ValueType value)
        {
            return (keyNameToValue.TryGetValue(nameOrAlias, out value)
                || keyAliasToValue.TryGetValue(nameOrAlias, out value));
        }

        public IEnumerator<KeyValuePair<string, ValueType>> GetEnumerator()
        {
            return keyNameToValue.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
