﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using EGSoft.Fc.EngineModel;
using EGSoft.Fc.SubspaceModel;
using EGSoft.Fc.SubspaceModel.Modules;

namespace EGSoft.Fc.CoreModel.Modules
{
    [FcEngineModuleInfo(
        ModuleName = "FcCore",
        ModuleAlias = "Fc",
        Author = "Fc",
        Version = "v1.0")]
    public class FcCore : IFcCoreParent
    {
        IFcEngineLinker linker;
        IFcCore fcCore;
        IMessenger messenger;

        Dictionary<String, IFcCore> moduleNameTofcCoreMap;
        Dictionary<String, IFcCore> aliasNameTofcCoreMap;

        public FcCore()
        {
            moduleNameTofcCoreMap = new Dictionary<string, IFcCore>(StringComparer.OrdinalIgnoreCase);
            aliasNameTofcCoreMap = new Dictionary<string, IFcCore>(StringComparer.OrdinalIgnoreCase);
        }

        //public IFcCommands Commands
        //{
        //    get { return commands; }
        //}

        public bool OnLoad(EngineModel.IFcEngineLinker linker)
        {
            fcCore = linker.AttachModule<IFcCore>();

            this.linker = linker;

            messenger = linker.AttachModule<IMessenger>();
            messenger.MessageReceived += new MessageReceivedHandle(messenger_MessageReceived);
            

            IFcCommand helpCmd = new FcCommand("help", "h", "Help command");
            helpCmd.Executed += new FcCommandExecutedHandle(helpCmd_Executed);
            fcCore.Commands.Register(helpCmd);

            IFcCommand aboutCmd = new FcCommand("about", "a", "About command");
            aboutCmd.Executed += new FcCommandExecutedHandle(aboutCmd_Executed);
            fcCore.Commands.Register(aboutCmd);

            return true;
        }

        void messenger_MessageReceived(object sender, MessageInfo msgInfo)
        {
            if (msgInfo.MessageType == MessageType.Broadcast
                || msgInfo.MessageType == MessageType.Server
                || msgInfo.MessageType == MessageType.Warning)
                return;

            String msg = msgInfo.Message;
            if (msg.Length > 0 && msg[0] == '!')
            {
                String moduleCmdKey = String.Empty, moduleCmdArgs = String.Empty;
                int firstSpace = msg.IndexOf(' ');
                if (firstSpace > 1)
                {
                    moduleCmdKey = msg.Substring(1, firstSpace - 1);
                    moduleCmdArgs = msg.Substring(firstSpace + 1);
                }
                else
                    moduleCmdKey = msg.Substring(1);

                String moduleKey = String.Empty, cmdKey = String.Empty;
                if (!String.IsNullOrEmpty(moduleCmdKey))
                {
                    int firstPeriod = moduleCmdKey.IndexOf('.');
                    if (firstPeriod > 1)
                    {
                        moduleKey = moduleCmdKey.Substring(0, firstPeriod);
                        if (moduleCmdKey.Length != firstPeriod + 1)
                            cmdKey = moduleCmdKey.Substring(firstPeriod + 1);
                    }
                    else
                        cmdKey = moduleCmdKey.Substring(firstPeriod + 1);
                }

                IFcCommands cmds;
                if (this.TryGetCommands(moduleKey, out cmds))
                {
                    IFcCommand cmd;
                    if (cmds.TryGetCommand(cmdKey, out cmd))
                    {
                        //p1:a b c p2:d f g
                        //p1:a b c p2:
                        //p1:abcp2:
                        String errorStr = String.Empty;
                        FcCommandArguments arguments = new FcCommandArguments();

                        if (!String.IsNullOrEmpty(moduleCmdArgs))
                        {
                            String pkey = String.Empty, pvalue = String.Empty;
                            int indexTemp = 0;
                            int indexA = moduleCmdArgs.IndexOf(':');
                            int indexB = -1;
                            if (indexA == -1)
                            {
                                //Make sure it takes in 1 parameter.
                            }
                            else
                            {
                                do
                                {
                                    pkey = moduleCmdArgs.Substring(indexTemp, indexA - indexTemp);
                                    if (pkey != String.Empty)
                                    {
                                        indexB = moduleCmdArgs.IndexOf(':', indexA + 1);
                                        if (indexB == -1)
                                        {
                                            pvalue = moduleCmdArgs.Substring(indexA + 1);
                                        }
                                        else
                                        {
                                            indexTemp = moduleCmdArgs.LastIndexOf(' ', indexB);
                                            if (indexTemp != -1)
                                            {
                                                if (indexTemp > indexA)
                                                {
                                                    pvalue = moduleCmdArgs.Substring(indexA + 1, indexTemp - indexA - 1);
                                                }
                                                else
                                                {
                                                    errorStr = "Invalid syntax. Usage: !moduleName.cmdName param1Name:value1 param2Name:value2 ...";
                                                }
                                            }
                                            else
                                            {
                                                //Invalid syntax.
                                                errorStr = "Invalid syntax. Usage: !moduleName.cmdName param1Name:value1 param2Name:value2 ...";
                                            }
                                        }
                                        Console.WriteLine("Key: " + pkey + " Value: " + pvalue);
                                    }
                                    else
                                    {
                                        //Invalid syntax.
                                        errorStr = "Invalid syntax. Usage: !moduleName.cmdName param1Name:value1 param2Name:value2 ...";
                                    }
                                    indexTemp++;
                                    indexA = indexB;
                                } while (indexA != -1 && errorStr == String.Empty);
                            }
                        }


                        //VALIDATE arguments.
                        if (errorStr != String.Empty)
                        {
                            messenger.SendPrivate(msgInfo.PlayerName, errorStr, SoundType.None);
                        }
                        else
                        {
                            cmd.Execute(this, new FcCommandContext(messenger, msgInfo, arguments));
                        }
                    }
                    else
                    {
                        //Command not found.
                        messenger.SendPrivate(msgInfo.PlayerName, "Unknown command for module " + moduleKey + ".", SoundType.None);
                    }
                }
                else
                {
                    messenger.SendPrivate(msgInfo.PlayerName, "Module not found. Use !fc.help to view all modules.", SoundType.None);
                    //Module not found.
                }
                
            }
        }

        private bool TryGetCommands(String moduleKey, out IFcCommands commands)
        {
            IFcCore child;
            if (moduleNameTofcCoreMap.TryGetValue(moduleKey, out child)
                || aliasNameTofcCoreMap.TryGetValue(moduleKey, out child))
            {
                commands = child.Commands;
                return true;
            }
            else
            {
                commands = null;
                return false;
            }
        }

        public bool OnUnload(EngineModel.IFcEngineLinker linker)
        {
            return true;
        }

        public EngineModel.IFcEngineModule GetChild(IFcEngineModuleInfo callerInfo)
        {
            IFcCore fcCoreChild;
            if (!moduleNameTofcCoreMap.TryGetValue(callerInfo.ModuleName, out fcCoreChild))
            {
                fcCoreChild = new FcCoreChild(callerInfo);
                moduleNameTofcCoreMap.Add(callerInfo.ModuleName, fcCoreChild);
                aliasNameTofcCoreMap.Add(callerInfo.ModuleAlias, fcCoreChild);
            }
            return fcCoreChild;
        }

        void aboutCmd_Executed(object sender, IFcCommandContext context)
        {
            
            context.Reply(" - ModuleName|Alias - ");
            foreach (IFcEngineModuleInfo mi in linker.ModuleInfoList)
            {
                String cmdNameStr;
                if (String.IsNullOrEmpty(mi.ModuleAlias))
                    cmdNameStr = mi.ModuleName;
                else
                    cmdNameStr = mi.ModuleName + "|" + mi.ModuleAlias;
                String miStr = String.Format("{0,20}: {1,-10} by {2,-19} ({3})", cmdNameStr, mi.Version, mi.Author, mi.Description);
                context.Reply(miStr);
            }
        }

        void helpCmd_Executed(object sender, IFcCommandContext context)
        {
            context.Reply(String.Format("{0,-20}-   {1}", "Module(s)", "Command(s)"));
            foreach(IFcCoreChild fcc in moduleNameTofcCoreMap.Values)
            {
                IFcEngineModuleInfo mi = fcc.ModfuleInfo;
                String cmdNameStr = String.Empty;
                if (String.IsNullOrEmpty(fcc.ModfuleInfo.ModuleAlias))
                    cmdNameStr = mi.ModuleName;
                else
                    cmdNameStr = mi.ModuleName + "|" + mi.ModuleAlias;

                String cmdsStr = String.Empty;
                foreach (IFcCommand cmd in fcc.Commands)
                    cmdsStr = cmdsStr + cmd.Name + "|" + cmd.Alias + ",";
                cmdsStr = cmdsStr.TrimEnd(',');
                String miStr = String.Format("{0,-20}.   {1}", cmdNameStr, cmdsStr);
                context.Reply(miStr);
            }
            context.Reply("-");
            context.Reply("Usage: !moduleName.cmdName or moduleAlias.cmdAlias (example: !fccore.help !fc.h !fccore.about !fc.a)");
        }

        public IFcCommands Commands
        {
            get { throw new NotImplementedException(); }
        }

        public IFcEngineModuleInfo ModfuleInfo
        {
            get { throw new NotImplementedException(); }
        }
    }
}
