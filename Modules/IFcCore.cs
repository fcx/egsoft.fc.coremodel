﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using EGSoft.Fc.EngineModel;

namespace EGSoft.Fc.CoreModel.Modules
{
    public interface IFcCore : IFcEngineModule
    {
        IFcCommands Commands
        {
            get;
        }

        IFcEngineModuleInfo ModfuleInfo { get; }
    }
}
