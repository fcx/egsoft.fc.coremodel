﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using EGSoft.Fc.EngineModel;

namespace EGSoft.Fc.CoreModel.Modules
{
    public class FcCoreChild : IFcCoreChild
    {
        IFcEngineModuleInfo moduleInfo;
        IFcCommands commands;

        public FcCoreChild(IFcEngineModuleInfo moduleInfo)
        {
            this.moduleInfo = moduleInfo;
            this.commands = new FcCommands();
        }

        public bool OnLoad(EngineModel.IFcEngineLinker linker)
        {
            throw new NotImplementedException();
        }

        public bool OnUnload(EngineModel.IFcEngineLinker linker)
        {
            throw new NotImplementedException();
        }

        public IFcCommands Commands
        {
            get { return commands; }
        }

        public IFcEngineModuleInfo ModfuleInfo
        {
            get { return moduleInfo; }
        }
    }
}
