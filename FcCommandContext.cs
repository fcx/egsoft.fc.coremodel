﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using EGSoft.Fc.SubspaceModel;
using EGSoft.Fc.SubspaceModel.Modules;

namespace EGSoft.Fc.CoreModel
{
    public class FcCommandContext : IFcCommandContext
    {
        IMessenger msger;
        MessageInfo msgInfo;

        IFcCommandArguments arguments;

        public string Username
        {
            get { return msgInfo.PlayerName; }
        }

        public IFcCommandArguments Arguments
        {
            get { return arguments; }
        }

        public FcCommandContext(IMessenger msger, MessageInfo msgInfo, IFcCommandArguments arguments)
        {
            this.msger = msger;
            this.msgInfo = msgInfo;
            this.arguments = arguments;
        }

        public void Reply(string message, SoundType soundType)
        {
            msger.SendPrivate(msgInfo.PlayerName, message, soundType);
        }

        public void Reply(string message)
        {
            Reply(message, SoundType.None);
        }


    }
}
