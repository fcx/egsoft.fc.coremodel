﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EGSoft.Fc.CoreModel
{
    public delegate void FcCommandExecutedHandle(object sender, IFcCommandContext context);

    public class FcCommand : IFcCommand
    {
        String name;
        String alias;
        String description;

        IFcCommandParameters parameters;

        public event FcCommandExecutedHandle Executed;

        public IFcCommandParameters Parameters
        {
            get { return parameters; }
        }
        
        public FcCommand(String name, String alias, String description)
        {
            if (String.IsNullOrEmpty(name))
                throw new Exception("Command name cannot be empty");

            this.name = name;
            this.alias = alias;
            this.description = description;
            this.parameters = new FcCommandParameters();
        }

        public void Execute(object sender, IFcCommandContext context)
        {
            Executed.Invoke(sender, context);
        }


        public string Name
        {
            get { return name; }
        }

        public string Alias
        {
            get { return alias; }
        }

        public string Description
        {
            get { return description; }
        }
    }
}
