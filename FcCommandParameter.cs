﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ComponentModel;

namespace EGSoft.Fc.CoreModel
{
    public class FcCommandParameter<PType> : IFcCommandParameter
    {
        public static TypeConverter Converter = TypeDescriptor.GetConverter(typeof(PType));

        bool isRequired;
        String name;
        String alias;
        String description;
        PType defaultValue;

        public string Name
        {
            get { return name; }
        }

        public string Alias
        {
            get { return alias; }
        }

        public string Description
        {
            get { return description; }
        }

        public object DefaultValue
        {
            get { return defaultValue; }
        }

        public bool IsRequired
        {
            get { return isRequired; }
        }

        public FcCommandParameter(String name, String alias, String description, PType defaultValue)
        {
            this.name = name;
            this.alias = alias;
            this.description = description;
            this.defaultValue = defaultValue;
            isRequired = false;
        }

        public FcCommandParameter(String name, String alias, String description)
        {
            this.name = name;
            this.alias = alias;
            this.description = description;
            this.defaultValue = default(PType);
            isRequired = true;
        }

        public bool TryParse(String input, out object result)
        {
            if (String.IsNullOrEmpty(input))
                result = defaultValue;
            else
            {
                try
                {
                    result = (PType)Converter.ConvertFromString(input);
                }
                catch
                {
                    result = default(PType);
                    return false;
                }
            }
            return true;
        }

        public override string ToString()
        {
            return this.Name + "|" + this.Alias + ":(" + this.Description + ")";
        }
    }
}
