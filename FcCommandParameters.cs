﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace EGSoft.Fc.CoreModel
{

    public class FcCommandParameters : IFcCommandParameters
    {
        int requireParamsCount;
        FcDictionary<IFcCommandParameter> paramDict;

        public FcCommandParameters()
        {
            requireParamsCount = 0;
            paramDict = new FcDictionary<IFcCommandParameter>();
        }

        public int Count
        {
            get { return paramDict.Count; }
        }

        public int CountRequired
        {
            get { return requireParamsCount; }
        }

        public IFcCommandParameter Add(IFcCommandParameter parameter)
        {
            if (parameter.IsRequired)
                requireParamsCount++;
            paramDict.Add(parameter.Name, parameter.Alias, parameter);
            return parameter;
        }

        public bool TryGetParameter(String nameOrAlias, out IFcCommandParameter parameter)
        {
            return paramDict.TryGetValue(nameOrAlias, out parameter);
        }

        public IFcCommandParameter Get(String nameOrAlias)
        {
            IFcCommandParameter parameter;
            paramDict.TryGetValue(nameOrAlias, out parameter);
            return parameter;
        }

        public IEnumerator<IFcCommandParameter> GetEnumerator()
        {
            return paramDict.Values.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
